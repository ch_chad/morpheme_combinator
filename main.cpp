//#include<stdio.h>
#include<iostream>
#include <fstream>
#include<string.h>
#include <vector>
using namespace std;
int read_file(char filename[],vector<string> &lines)
{
    fstream f(filename);
    if (!f)
    {
        return -1;
    }
    string tmp;
    while(getline(f, tmp))
    {
        lines.push_back(tmp);
    }
    return lines.size();
}

int find_prefix(vector<string> arr, string word, int startline)
{
    for(int i=startline; i<arr.size(); i++)
    {
        if(arr[i]==word.substr(0, arr[i].length()))//starts with arr[i]
            return i;
    }
    return -1;
}

int find_post(vector<string> arr,string word, int startline)
{
    for(int i=startline; i<arr.size(); i++)
    {
        if(arr[i]==word)
            return i;
    }
    return -1;
}

void print_morf(vector<string> arr)
{
    for(int i=0; i<arr.size(); i++)
        cout << arr[i] << "\n";
    cout << "\n";
}

int main()
{
    char filePref[]="pref.txt";
    char fileRoot[]="root.txt";
    char filePost[]="post.txt";
    vector<string> pref;
    vector<string> root;
    vector<string> post;
    if(read_file(filePref,pref)==-1||read_file(fileRoot,root)==-1||read_file(filePost,post)==-1)
    {
        cout << "Error: file isn't found" << "\n";
        return 0;
    }
    print_morf(pref);
    print_morf(root);
    print_morf(post);
    string word;
    cout << "Enter your word" << "\n";
    cin >> word;
    string word_pref;
    string word_pref_root;
    int indexpref=-1;
    int indexroot=-1;
    int WHYSOSERIOUS=0;
    int indexpost;
    int i=1;
    int j=1;

    while(i==1)
    {
        int startline=indexpref+1;
        indexpref=find_prefix(pref, word, startline);
        if(indexpref==-1)
        {
            startline=0;
            cout << "Maybe, there is a zero prefix" << "\n";
            int indexroot=find_prefix(root,word,startline);
            if(indexroot==-1)
            {

                cout << "Cannot find such a root in my dictionary" << "\n" << "This word isn't in my dictionary" << "\n";
                i=2;
                break;
            }
            cout << "All right, there is a zero prefix" << "\n";
            cout << "Root is " << root[indexroot] << "\n";
            //char*word_root=word+strlen(root[indexroot]);//TODO: whats this?
            if (word==root[indexroot]) cout << "There is a zero ending\n"; //��������� indexpref=-1, ����� ����� ������������ ��� ������� ���������
            else cout << "Postfix is " << word.substr(root[indexroot].length(), string::npos/*to the end*/) << "\n";
            WHYSOSERIOUS=indexroot;
            cout << " >" << WHYSOSERIOUS << "<"  << "> " << indexroot << " <" << endl;
            break;
        }
        else cout << "Prefix is " << pref[indexpref] << "\n";
        word_pref=word.substr(pref[indexpref].length(), string::npos);
        cout << word_pref << "\n";
        while(j==1)
        {
            startline=indexroot+1;
            indexroot=find_prefix(root,word_pref,startline);
            if(indexroot==-1)break;
            else cout << "Root is " << root[indexroot] << "\n";
            if (pref[indexpref].length()+root[indexroot].length() == word.length())
            {
                cout << "There is a zero ending\n";
                break;
            }
            else
            {
                string word_pref_root=word.substr(pref[indexpref].length()-1+root[indexroot].length()-1, string::npos);
                cout << "Postfix is " << word_pref_root << "\n";
                indexpost=find_post(post,word_pref_root,0);
                if(indexpost==-1) j=1;
                else
                {
                    cout << "Ending is " << post[indexpost] << "\n";
                    j=2;
                    i=2;
                }
            }
        }
        if(indexroot==-1) i=1;
    }

    cout << " >" << WHYSOSERIOUS << "<"  << "> " << indexroot << " <" << endl;

    if(WHYSOSERIOUS==-1) cout << "There isn't this word in my dictionary" << "\n";
    else
    {
        cout << "The word is identified" << "\n";
        if(indexpref==-1) cout << "Prefix is zero" << "\n";
        else cout << "Prefix is " << pref[indexpref] << "\n";
        cout << "Root is " << root[WHYSOSERIOUS] << "\n";
        if(indexpref==-1) cout << "Ending is zero\n";
        else cout << "Postfix is " << post[indexpost] << "\n";
    }
}
